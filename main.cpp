#include <QApplication>
#include <QWidget>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QWidget window;
    QObject o;

    window.resize(800, 600);
    window.setWindowTitle("Sample Window");
    window.show();

    return app.exec();
}
